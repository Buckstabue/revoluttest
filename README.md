Implemented a test task for Revolut Ltd.  
The task is to create a currency converter.
The app must download and update rates every 1 second using API  
https://revolut.duckdns.org/latest?base=EUR  
List all currencies you get from the endpoint (one per row). Each row has an input where you
can enter any amount of money. When you tap on currency row it should slide to top and its
input becomes first responder. When you�re changing the amount the app must simultaneously
update the corresponding value for other currencies.  
Video demo: https://youtu.be/omcS-6LeKoo 
-------------
Libraries: moxy, dagger 2, retrofit, okhttp, rxJava
