package com.buckstabue.revoluttestproject.di

import com.buckstabue.revoluttestproject.App
import com.buckstabue.revoluttestproject.di.component.AppComponent
import com.buckstabue.revoluttestproject.di.component.DaggerAppComponent
import com.buckstabue.revoluttestproject.di.component.RepositoryComponent
import com.buckstabue.revoluttestproject.di.module.AppModule
import com.buckstabue.revoluttestproject.di.module.NetworkModule


class AppInjector {
    companion object {
        private val appComponent: AppComponent by lazy {
            DaggerAppComponent.builder()
                    .appModule(AppModule(App.context))
                    .networkModule(NetworkModule())
                    .build()
        }

        val repositoryComponent: RepositoryComponent by lazy {
            appComponent.plusRepositoryComponent()
        }
    }
}