package com.buckstabue.revoluttestproject.di.component

import com.buckstabue.domain.di.scope.PerScreen
import com.buckstabue.revoluttestproject.di.module.CurrencyConverterModule
import com.buckstabue.revoluttestproject.ui.fragment.CurrencyConverterFragment
import dagger.Subcomponent

@PerScreen
@Subcomponent(modules = arrayOf(CurrencyConverterModule::class))
interface CurrencyConverterComponent {
    fun inject(currencyConverterFragment: CurrencyConverterFragment)
}