package com.buckstabue.revoluttestproject.di.component

import com.buckstabue.domain.di.scope.PerSession
import com.buckstabue.revoluttestproject.di.module.RepositoryModule
import dagger.Subcomponent

@PerSession
@Subcomponent(modules = arrayOf(RepositoryModule::class))
interface RepositoryComponent {

    fun plusCurrencyConverterComponent(): CurrencyConverterComponent
}