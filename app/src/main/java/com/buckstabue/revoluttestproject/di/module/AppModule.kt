package com.buckstabue.revoluttestproject.di.module

import android.content.Context
import com.buckstabue.domain.di.scope.PerApplication
import com.buckstabue.revoluttestproject.util.rx.AppScheduler
import com.buckstabue.revoluttestproject.util.rx.AppSchedulerImpl
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@Module
class AppModule(private val context: Context) {
    @PerApplication
    @Provides
    fun provideContext(): Context = context

    @PerApplication
    @Provides
    fun provideAppScheduler(): AppScheduler =
            AppSchedulerImpl(io = Schedulers.io(), mainThread = AndroidSchedulers.mainThread())
}