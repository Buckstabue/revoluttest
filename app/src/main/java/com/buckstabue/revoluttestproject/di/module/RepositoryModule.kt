package com.buckstabue.revoluttestproject.di.module

import com.buckstabue.data.repository.CurrencyRepository
import com.buckstabue.data.repository.CurrencyRepositoryImpl
import com.buckstabue.domain.di.scope.PerSession
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {

    @PerSession
    @Binds
    abstract fun bindCurrencyRepository(impl: CurrencyRepositoryImpl): CurrencyRepository
}