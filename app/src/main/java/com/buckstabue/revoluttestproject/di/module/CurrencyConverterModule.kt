package com.buckstabue.revoluttestproject.di.module

import com.buckstabue.revoluttestproject.ui.adapter.CurrencyConverterAdapter
import com.buckstabue.revoluttestproject.ui.presenter.CurrencyConverterPresenter
import dagger.Binds
import dagger.Module

@Module
abstract class CurrencyConverterModule {
    @Binds
    abstract fun bindAdapterCallback(impl: CurrencyConverterPresenter): CurrencyConverterAdapter.Callback
}