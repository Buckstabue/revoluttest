package com.buckstabue.revoluttestproject.di.component

import com.buckstabue.domain.di.scope.PerApplication
import com.buckstabue.revoluttestproject.di.module.AppModule
import com.buckstabue.revoluttestproject.di.module.NetworkModule
import dagger.Component

@PerApplication
@Component(modules = arrayOf(AppModule::class, NetworkModule::class))
interface AppComponent {
    fun plusRepositoryComponent(): RepositoryComponent
}