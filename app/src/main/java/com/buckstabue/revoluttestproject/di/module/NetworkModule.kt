package com.buckstabue.revoluttestproject.di.module

import com.buckstabue.data.api.CurrencyApiInterface
import com.buckstabue.domain.di.scope.PerApplication
import com.buckstabue.revoluttestproject.BuildConfig
import com.buckstabue.revoluttestproject.util.AppConst
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class NetworkModule {
    @PerApplication
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
                .setLevel(if (BuildConfig.DEBUG) Level.BODY else Level.NONE)
        return OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .addInterceptor(loggingInterceptor)
                .build()
    }

    @PerApplication
    @Provides
    fun provideGson(): Gson = GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
            .create()


    @PerApplication
    @Provides
    fun provideCallAdapterFactory(): CallAdapter.Factory = RxJava2CallAdapterFactory.create()

    @PerApplication
    @Provides
    fun provideConverterFactory(): Converter.Factory = GsonConverterFactory.create()

    @PerApplication
    @Provides
    fun provideRestAdapter(converterFactory: Converter.Factory,
                           callAdapterFactory: CallAdapter.Factory,
                           okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(AppConst.API_URL)
                .addConverterFactory(converterFactory)
                .client(okHttpClient)
                .addCallAdapterFactory(callAdapterFactory)
                .build()
    }

    @PerApplication
    @Provides
    fun provideUserApiInterface(retrofit: Retrofit): CurrencyApiInterface
            = retrofit.create(CurrencyApiInterface::class.java)
}