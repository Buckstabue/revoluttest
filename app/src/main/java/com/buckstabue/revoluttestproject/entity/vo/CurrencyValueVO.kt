package com.buckstabue.revoluttestproject.entity.vo

data class CurrencyValueVO(val name: String,
                           val value: String)