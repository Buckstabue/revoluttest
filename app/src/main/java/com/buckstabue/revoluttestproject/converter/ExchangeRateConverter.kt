package com.buckstabue.revoluttestproject.converter

import com.buckstabue.data.entity.dto.ExchangeReferenceDTO
import com.buckstabue.domain.di.scope.PerApplication
import com.buckstabue.revoluttestproject.entity.vo.CurrencyValueVO
import java.math.BigDecimal
import java.math.RoundingMode
import javax.inject.Inject

@PerApplication
class ExchangeRateConverter @Inject constructor() {
    fun map(dto: ExchangeReferenceDTO, currencyValue: Double): MutableList<CurrencyValueVO> {
        val list = ArrayList<CurrencyValueVO>(dto.rates.size + 1)
        dto.rates.forEach {
            list.add(calculateCurrencyValue(it.key, it.value, currencyValue))
        }
        list.add(0, CurrencyValueVO(dto.base, formatCurrencyValue(currencyValue)))
        return list
    }

    private fun calculateCurrencyValue(name: String, rate: Double, multiplyingValue: Double): CurrencyValueVO {
        val value = rate * multiplyingValue
        return CurrencyValueVO(
                name = name,
                value = formatCurrencyValue(value))
    }

    private fun formatCurrencyValue(value: Double): String {
        val placesAfterPoint = 2
        var bigDecimal = BigDecimal(value)
        bigDecimal = bigDecimal.setScale(placesAfterPoint, RoundingMode.FLOOR)
        val roundedValue = bigDecimal.toDouble()
        if (roundedValue == 0.0) {
            return ""
        } else {
            return String.format("%.2f", roundedValue)
        }
    }
}