package com.buckstabue.revoluttestproject.ui.view

import com.buckstabue.revoluttestproject.entity.vo.CurrencyValueVO

interface CurrencyConverterView : BaseView {
    fun showCurrencies(items: List<CurrencyValueVO>)
}