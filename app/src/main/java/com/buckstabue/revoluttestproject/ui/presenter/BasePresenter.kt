package com.buckstabue.revoluttestproject.ui.presenter

import android.support.annotation.CallSuper
import com.arellomobile.mvp.MvpPresenter
import com.buckstabue.revoluttestproject.BuildConfig
import com.buckstabue.revoluttestproject.model.BasePresenterDependency
import com.buckstabue.revoluttestproject.ui.view.BaseView
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subscribers.DisposableSubscriber

abstract class BasePresenter<V : BaseView>(basePresenterDependency: BasePresenterDependency) : MvpPresenter<V>() {
    protected val context = basePresenterDependency.context
    private val appScheduler = basePresenterDependency.appScheduler
    private val compositeDisposable = CompositeDisposable()

    protected fun addSubscription(subscription: Disposable) {
        compositeDisposable.add(subscription)
    }

    @CallSuper
    override fun attachView(view: V) {
        super.attachView(view)
        onViewAttached()
    }

    @CallSuper
    override fun detachView(view: V) {
        super.detachView(view)
        onViewDetached()
    }

    protected open fun onViewAttached() {

    }

    protected open fun onViewDetached() {

    }

    @CallSuper
    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    protected fun <T : Any> safeExecute(flowable: Flowable<T>, subscriber: RevolutSubscriber<T>): Disposable {
        val subscription = execute(flowable, subscriber)
        addSubscription(subscription)
        return subscription
    }

    private fun processUnhandledException(e: Throwable) {
        if (BuildConfig.DEBUG) {
            e.printStackTrace()
        }
        viewState.showCommonError(e.toString())
    }

    protected fun <T : Any> execute(observable: Flowable<T>, subscriber: RevolutSubscriber<T>): Disposable {
        return observable.subscribeOn(appScheduler.io)
                .observeOn(appScheduler.mainThread)
                .doFinally { subscriber.doFinally() }
                .subscribeWith(WrappedObserver(subscriber))
    }

    private inner class WrappedObserver<T : Any>(private val subscriber: RevolutSubscriber<T>) : DisposableSubscriber<T>() {
        override fun onError(e: Throwable) {
            if (!subscriber.handleError(e)) {
                processUnhandledException(e)
            }
        }

        override fun onStart() {
            super.onStart()
            subscriber.onStart()
        }

        override fun onNext(t: T) {
            subscriber.onNext(t)
        }

        override fun onComplete() {
            subscriber.onComplete()
        }
    }
}

interface BaseRevolutSubscriber {
    /**
     * Called once when observable is terminated(unsubscribed or a terminal event is sent)
     */
    fun doFinally() {}

    /**
     * @return true if you process the error, or false if can't handle it and it will be processed in the general way
     */
    fun handleError(e: Throwable): Boolean
}

interface RevolutSubscriber<in T : Any> : BaseRevolutSubscriber {
    fun onNext(t: T)
    fun onComplete()
    fun onStart() {}
}
