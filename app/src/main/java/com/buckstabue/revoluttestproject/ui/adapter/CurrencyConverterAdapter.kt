package com.buckstabue.revoluttestproject.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import butterknife.BindView
import com.buckstabue.domain.di.scope.PerScreen
import com.buckstabue.revoluttestproject.R
import com.buckstabue.revoluttestproject.entity.vo.CurrencyValueVO
import com.buckstabue.revoluttestproject.ui.adapter.CurrencyConverterAdapter.CurrencyViewHolder
import com.buckstabue.revoluttestproject.ui.adapter.viewholder.BaseViewHolder
import com.buckstabue.revoluttestproject.util.listeneradapter.TextWatcherAdapter
import javax.inject.Inject

@PerScreen
class CurrencyConverterAdapter @Inject constructor(
        var callback: Callback?) : RecyclerView.Adapter<CurrencyViewHolder>() {

    var items: List<CurrencyValueVO> = emptyList()

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        return CurrencyViewHolder(parent)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class CurrencyViewHolder(parent: ViewGroup) : BaseViewHolder(parent, R.layout.item_currency_value) {
        @BindView(R.id.name)
        lateinit var nameTextView: TextView

        @BindView(R.id.value)
        lateinit var valueEditText: EditText

        init {
            itemView.setOnClickListener {
                valueEditText.requestFocus()
            }
            valueEditText.setOnFocusChangeListener { _, hasFocus ->
                if (exists && hasFocus) {
                    notifyItemSelected()
                }
            }
            valueEditText.addTextChangedListener(object : TextWatcherAdapter() {

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (adapterPosition == 0) {
                        callback?.onCurrencyValueChanged(s.toString())
                    }
                }
            })
        }

        private fun notifyItemSelected() {
            if (!this.exists) {
                return
            }
            val position = adapterPosition
            val item = items[position]
            callback?.onItemSelected(position, item)
        }


        fun bind(vo: CurrencyValueVO) {
            nameTextView.text = vo.name
            valueEditText.setText(vo.value)
        }
    }

    interface Callback {
        fun onItemSelected(position: Int, currencyValue: CurrencyValueVO)
        fun onCurrencyValueChanged(value: String)
    }
}