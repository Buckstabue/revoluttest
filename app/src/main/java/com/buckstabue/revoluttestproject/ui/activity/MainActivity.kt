package com.buckstabue.revoluttestproject.ui.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.buckstabue.revoluttestproject.R
import com.buckstabue.revoluttestproject.ui.fragment.CurrencyConverterFragment
import com.buckstabue.revoluttestproject.util.findByTag

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showCurrencyConverterScreen()
    }

    private fun showCurrencyConverterScreen() {
        var fragment = supportFragmentManager.findByTag<CurrencyConverterFragment>(CurrencyConverterFragment.TAG)
        if (fragment == null) {
            fragment = CurrencyConverterFragment.newInstance()
        }
        if (!fragment.isAdded) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.content, fragment, CurrencyConverterFragment.TAG)
                    .commitAllowingStateLoss()
        }
    }
}
