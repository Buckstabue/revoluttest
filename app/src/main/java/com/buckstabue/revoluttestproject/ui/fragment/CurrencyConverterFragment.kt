package com.buckstabue.revoluttestproject.ui.fragment

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.View
import butterknife.BindView
import butterknife.ButterKnife
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.buckstabue.revoluttestproject.R
import com.buckstabue.revoluttestproject.di.AppInjector
import com.buckstabue.revoluttestproject.di.component.CurrencyConverterComponent
import com.buckstabue.revoluttestproject.entity.vo.CurrencyValueVO
import com.buckstabue.revoluttestproject.ui.adapter.CurrencyConverterAdapter
import com.buckstabue.revoluttestproject.ui.presenter.CurrencyConverterPresenter
import com.buckstabue.revoluttestproject.ui.view.CurrencyConverterView
import javax.inject.Inject

class CurrencyConverterFragment : BaseFragment<CurrencyConverterComponent>(), CurrencyConverterView {
    companion object {
        val TAG = CurrencyConverterFragment::class.java.simpleName

        fun newInstance() = CurrencyConverterFragment()
    }

    @BindView(R.id.currencies)
    lateinit var recyclerView: RecyclerView

    @InjectPresenter
    @Inject
    @get:ProvidePresenter
    override lateinit var presenter: CurrencyConverterPresenter

    @Inject
    protected lateinit var adapter: CurrencyConverterAdapter

    private var pendingAnimationFinishItems: List<CurrencyValueVO> = emptyList()

    override fun showCurrencies(items: List<CurrencyValueVO>) {
        pendingAnimationFinishItems = items
        recyclerView.itemAnimator.isRunning {
            val diffCallback = CurrencyDiffUtilCallback(adapter.items, items)
            val diffResult = DiffUtil.calculateDiff(diffCallback)
            adapter.items = ArrayList(pendingAnimationFinishItems)
            recyclerView.post {
                diffResult.dispatchUpdatesTo(adapter)
            }
        }
    }

    override fun provideComponent(): CurrencyConverterComponent {
        return AppInjector.repositoryComponent.plusCurrencyConverterComponent()
    }

    override fun injectPresenter(component: CurrencyConverterComponent) {
        component.inject(this)
    }

    override fun onViewCreatedImpl(view: View) {
        ButterKnife.bind(this, view)
        recyclerView.adapter = adapter
        recyclerView.itemAnimator.changeDuration = 0
    }

    override fun getLayoutResId() = R.layout.fragment_currency_converter

    private class CurrencyDiffUtilCallback(private val oldList: List<CurrencyValueVO>,
                                           private val newList: List<CurrencyValueVO>) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].name == newList[newItemPosition].name
        }

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            if (oldItemPosition == 0 && newItemPosition == 0) {
                return true
            }
            return oldList[oldItemPosition].value == newList[newItemPosition].value
        }

    }
}