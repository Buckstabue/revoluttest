package com.buckstabue.revoluttestproject.ui.presenter

import com.arellomobile.mvp.InjectViewState
import com.buckstabue.data.entity.dto.ExchangeReferenceDTO
import com.buckstabue.domain.di.scope.PerScreen
import com.buckstabue.domain.interactor.ObserveCurrencyReferenceInteractor
import com.buckstabue.revoluttestproject.converter.ExchangeRateConverter
import com.buckstabue.revoluttestproject.entity.vo.CurrencyValueVO
import com.buckstabue.revoluttestproject.model.BasePresenterDependency
import com.buckstabue.revoluttestproject.ui.adapter.CurrencyConverterAdapter
import com.buckstabue.revoluttestproject.ui.view.CurrencyConverterView
import com.buckstabue.revoluttestproject.util.rx.SafeDisposableDelegate
import io.reactivex.Flowable
import javax.inject.Inject

@PerScreen
@InjectViewState
class CurrencyConverterPresenter @Inject constructor(
        basePresenterDependency: BasePresenterDependency,
        private val observeCurrenciesInteractor: ObserveCurrencyReferenceInteractor,
        private val exchangeRateConverter: ExchangeRateConverter) :
        BasePresenter<CurrencyConverterView>(basePresenterDependency), CurrencyConverterAdapter.Callback {
    private val REFRESH_RATE_MS = 1000L

    private var selectedCurrency = ""
    private var latestLoadedCurrencies = mutableListOf<CurrencyValueVO>()

    private var currencyReferenceSubscriber by SafeDisposableDelegate()
    private var refreshCurrenciesSubscriber by SafeDisposableDelegate()

    private val orderedCurrencyList = mutableListOf<String>()

    @Volatile
    private var currencyInput = 100.0

    @Volatile
    private var latestCurrencyReference: ExchangeReferenceDTO? = null

    override fun onViewAttached() {
        observeCurrencyReference(selectedCurrency)
    }

    override fun onViewDetached() {
        currencyReferenceSubscriber.dispose()
    }

    private fun observeCurrencyReference(base: String) {
        val observable = observeCurrenciesInteractor.execute(base, REFRESH_RATE_MS)
                .doOnNext { latestCurrencyReference = it }
                .map { exchangeRateConverter.map(it, currencyInput) }
                .doOnNext { sortByUserPosition(it) }
        currencyReferenceSubscriber = safeExecute(observable, GetCurrencyListSubscriber())
    }

    private fun sortByUserPosition(items: MutableList<CurrencyValueVO>) {
        if (orderedCurrencyList.isEmpty() && items.isNotEmpty()) { // happens when loaded items for the first time
            orderedCurrencyList.add(items[0].name) // the first item in the initially loaded list is the currency base
        }
        val currencyMap = HashMap<String, Int>(orderedCurrencyList.size)
        orderedCurrencyList.forEachIndexed { index, currency ->
            currencyMap.put(currency, index)
        }
        items.sortWith(Comparator { o1, o2 ->
            if (currencyMap.containsKey(o1.name) && currencyMap.containsKey(o2.name)) {
                currencyMap[o1.name]!! - currencyMap[o2.name]!!
            } else if (currencyMap.containsKey(o1.name)) {
                -1
            } else if (currencyMap.containsKey(o2.name)) {
                1
            } else {
                o1.name.compareTo(o2.name)
            }
        })
        // items are sorted so that new elements are always in the tail of the list
        val newCurrencies = items.takeLastWhile { !currencyMap.containsKey(it.name) }.map { it.name }
        orderedCurrencyList.addAll(newCurrencies)
    }

    override fun onItemSelected(position: Int, vo: CurrencyValueVO) {
        if (position == 0) {
            return
        }
        currencyReferenceSubscriber.dispose()
        latestCurrencyReference = null
        selectedCurrency = vo.name
        currencyInput = parseCurrencyValue(vo.value)
        swapListItems(position, 0, orderedCurrencyList)
        swapListItems(position, 0, latestLoadedCurrencies)
        viewState.showCurrencies(latestLoadedCurrencies)
        observeCurrencyReference(vo.name)
    }

    private fun <T : Any> swapListItems(pos1: Int, pos2: Int, list: MutableList<T>) {
        val temp = list[pos1]
        list[pos1] = list[pos2]
        list[pos2] = temp
    }

    override fun onCurrencyValueChanged(value: String) {
        currencyInput = parseCurrencyValue(value)
        refreshCurrencies()
    }

    private fun refreshCurrencies() {
        val refreshObservable = Flowable.fromCallable {
            val dto = latestCurrencyReference
            if (dto != null) {
                exchangeRateConverter.map(dto, currencyInput)
            } else {
                // if we have not downloaded the currency reference, show empty values
                latestLoadedCurrencies.map {
                    if (it.name == selectedCurrency) {
                        it
                    } else {
                        CurrencyValueVO(it.name, "")
                    }
                }.toMutableList()
            }
        }.doOnNext {
            sortByUserPosition(it)
        }
        refreshCurrenciesSubscriber = safeExecute(refreshObservable, RefreshCurrencyListSubscriber())
    }

    private fun parseCurrencyValue(value: String): Double {
        return value.toDoubleOrNull() ?: 0.0
    }

    private inner class GetCurrencyListSubscriber : RevolutSubscriber<List<CurrencyValueVO>> {
        override fun onNext(items: List<CurrencyValueVO>) {
            latestLoadedCurrencies = items.toMutableList()
            viewState.showCurrencies(items)
        }

        override fun onComplete() {
        }

        override fun handleError(e: Throwable) = false
    }

    private inner class RefreshCurrencyListSubscriber : RevolutSubscriber<List<CurrencyValueVO>> {
        override fun handleError(e: Throwable): Boolean = false

        override fun onNext(items: List<CurrencyValueVO>) {
            viewState.showCurrencies(items)
        }

        override fun onComplete() {
        }
    }
}