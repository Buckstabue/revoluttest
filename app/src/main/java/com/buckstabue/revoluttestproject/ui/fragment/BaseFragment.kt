package com.buckstabue.revoluttestproject.ui.fragment

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.buckstabue.revoluttestproject.ui.view.BaseView
import com.buckstabue.revoluttestproject.util.moxy.AppMvpDelegate

abstract class BaseFragment<ComponentType : Any> : MvpAppCompatFragment(), BaseView {
    abstract val presenter: Any

    private val _mvpDelegate by lazy { AppMvpDelegate(this) }


    override fun onCreate(savedInstanceState: Bundle?) {
        mvpDelegate.initDelegateTag(savedInstanceState)
        super.onCreate(savedInstanceState)
    }

    override final fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
            = inflater.inflate(getLayoutResId(), container, false)

    override final fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        onViewCreatedImpl(view)
        super.onViewCreated(view, savedInstanceState)
    }

    abstract fun provideComponent(): ComponentType

    /**
     * Should be called after super.onCreate() method call
     */
    protected fun getComponent(): ComponentType {
        return mvpDelegate.getComponent()
    }

    abstract fun injectPresenter(component: ComponentType)

    abstract fun onViewCreatedImpl(view: View)

    @LayoutRes
    abstract fun getLayoutResId(): Int

    override fun showCommonError(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override final fun toString(): String {
        // finalize since it's used for generating tags for presenter store
        return super.toString()
    }

    override final fun getMvpDelegate() = _mvpDelegate
}