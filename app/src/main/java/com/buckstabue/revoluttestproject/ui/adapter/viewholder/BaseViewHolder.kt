package com.buckstabue.revoluttestproject.ui.adapter.viewholder;

import android.content.Context
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import butterknife.ButterKnife

abstract class BaseViewHolder(parent: ViewGroup, @LayoutRes layoutResId: Int) :
        RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(layoutResId, parent, false)) {
    init {
        ButterKnife.bind(this, itemView)
    }

    protected val context: Context
        get() = itemView.context

    val exists
        get() = adapterPosition != RecyclerView.NO_POSITION
}