package com.buckstabue.revoluttestproject.util.rx

import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class SafeDisposableDelegate : ReadWriteProperty<Any, Disposable> {
    private var disposable: Disposable = Disposables.disposed()

    override fun getValue(thisRef: Any, property: KProperty<*>) = disposable

    override fun setValue(thisRef: Any, property: KProperty<*>, value: Disposable) {
        disposable.dispose()
        disposable = value
    }
}
