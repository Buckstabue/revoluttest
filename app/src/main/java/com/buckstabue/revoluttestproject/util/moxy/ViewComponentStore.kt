package com.buckstabue.revoluttestproject.util.moxy

object ViewComponentStore {
    private val tagToComponent = mutableMapOf<String, Any>()

    fun addComponent(tag: String, component: Any) {
        tagToComponent.put(tag, component)
    }

    /**
     * @return cached component or null
     */
    fun <T : Any> getComponent(tag: String) = tagToComponent[tag] as T?

    fun removeComponent(tag: String) {
        if (!tagToComponent.containsKey(tag)) {
            // TODO add ErrorUtils.handleError()
        }
        tagToComponent.remove(tag)
    }
}