package com.buckstabue.revoluttestproject.util.rx

import io.reactivex.Scheduler

class AppSchedulerImpl(override val io: Scheduler,
                       override val mainThread: Scheduler) : AppScheduler