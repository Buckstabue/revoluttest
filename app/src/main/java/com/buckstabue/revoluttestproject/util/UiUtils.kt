package com.buckstabue.revoluttestproject.util

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

fun <T : Fragment?> FragmentManager.findByTag(tag: String): T? {
    return this.findFragmentByTag(tag) as T?
}