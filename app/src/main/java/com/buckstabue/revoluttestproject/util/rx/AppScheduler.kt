package com.buckstabue.revoluttestproject.util.rx

import io.reactivex.Scheduler

interface AppScheduler {
    val io: Scheduler
    val mainThread: Scheduler
}