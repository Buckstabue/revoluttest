package com.buckstabue.revoluttestproject.util.moxy

import android.os.Bundle
import com.arellomobile.mvp.MvpDelegate
import com.buckstabue.revoluttestproject.ui.fragment.BaseFragment


class AppMvpDelegate<ComponentType : Any, T : BaseFragment<ComponentType>>(private val delegate: T) :
        MvpDelegate<T>(delegate) {

    private lateinit var delegateTag: String

    fun initDelegateTag(bundle: Bundle?) {
        delegateTag = getDelegateTag(bundle)
    }

    override fun onCreate(bundle: Bundle?) {
        var component: ComponentType? = ViewComponentStore.getComponent(delegateTag)
        if (component == null) {
            component = delegate.provideComponent()
            ViewComponentStore.addComponent(delegateTag, component)
        }
        delegate.injectPresenter(component)
        super.onCreate(bundle)
    }

    override fun onDestroy() {
        super.onDestroy()
        ViewComponentStore.removeComponent(delegateTag)
    }

    private fun getDelegateTag(savedInstanceState: Bundle?): String {
        val MOXY_BUNDLE_TAG = MvpDelegate.MOXY_DELEGATE_TAGS_KEY
        val MOXY_KEY_TAG = "com.arellomobile.mvp.MvpDelegate.KEY_TAG"

        if (savedInstanceState == null || !savedInstanceState.containsKey(MOXY_BUNDLE_TAG)) {
            return generateDelegateTag()
        }
        val bundle = savedInstanceState.getBundle(MOXY_BUNDLE_TAG)
        return bundle.getString(MOXY_KEY_TAG)
    }

    override final fun toString(): String {
        // finalize implementation since caching presenters and components is relied on this method
        return super.toString()
    }

    // copy paste from MvpDelegate#generateTag()
    private fun generateDelegateTag(): String =
            delegate.javaClass.simpleName + "$" + javaClass.simpleName + this.toString().replace(javaClass.name, "")

    fun getComponent(): ComponentType {
        return ViewComponentStore.getComponent<ComponentType>(delegateTag)!!
    }
}