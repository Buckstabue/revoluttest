package com.buckstabue.revoluttestproject.util.moxy

import android.os.Bundle
import com.arellomobile.mvp.MvpDelegate
import com.arellomobile.mvp.MvpFacade

object MoxyUtils {
    fun isPresenterStored(savedInstanceState: Bundle?): Boolean {
        val MOXY_BUNDLE_TAG = MvpDelegate.MOXY_DELEGATE_TAGS_KEY
        val MOXY_KEY_TAG = "com.arellomobile.mvp.MvpDelegate.KEY_TAG"

        if (savedInstanceState == null || !savedInstanceState.containsKey(MOXY_BUNDLE_TAG)) {
            return false
        }
        val bundle = savedInstanceState.getBundle(MOXY_BUNDLE_TAG)
        if (!bundle.containsKey(MOXY_KEY_TAG)) {
            return false
        }
        val presenterTag = bundle.getString(MOXY_KEY_TAG) + "\$presenter"
        return MvpFacade.getInstance().presenterStore.get(presenterTag) != null
    }
}