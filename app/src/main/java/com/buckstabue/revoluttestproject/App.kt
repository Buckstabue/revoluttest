package com.buckstabue.revoluttestproject

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.squareup.leakcanary.LeakCanary

class App : Application() {
    companion object {
        @SuppressLint("StaticFieldLeak")
        internal lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        }
        context = applicationContext
        LeakCanary.install(this)
    }
}