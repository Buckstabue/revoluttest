package com.buckstabue.revoluttestproject.model

import android.content.Context
import com.buckstabue.domain.di.scope.PerApplication
import com.buckstabue.revoluttestproject.util.rx.AppScheduler
import javax.inject.Inject

@PerApplication
class BasePresenterDependency @Inject constructor(val context: Context,
                                                  val appScheduler: AppScheduler)