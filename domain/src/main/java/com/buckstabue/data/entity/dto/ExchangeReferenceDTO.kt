package com.buckstabue.data.entity.dto

import com.google.gson.annotations.SerializedName

data class ExchangeReferenceDTO(
        @SerializedName("base")
        val base: String,

        @SerializedName("rates")
        val rates: Map<String, Double>
)
