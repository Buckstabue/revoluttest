package com.buckstabue.data.repository

import com.buckstabue.data.entity.dto.ExchangeReferenceDTO
import io.reactivex.Single

interface CurrencyRepository {
    fun getExchangeReference(base: String): Single<ExchangeReferenceDTO>
}