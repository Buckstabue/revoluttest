package com.buckstabue.domain.di.scope

@javax.inject.Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerSession