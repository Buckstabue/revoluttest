package com.buckstabue.domain.interactor

import com.buckstabue.data.entity.dto.ExchangeReferenceDTO
import com.buckstabue.data.repository.CurrencyRepository
import com.buckstabue.domain.di.scope.PerScreen
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@PerScreen
class ObserveCurrencyReferenceInteractor @Inject constructor(
        private val currencyRepository: CurrencyRepository) {
    fun execute(base: String, refreshPeriodMs: Long): Flowable<ExchangeReferenceDTO> {
        return Flowable.just(0L) // start observing immediately. It's added because .interval() has an initial delay
                .concatWith(Flowable.interval(refreshPeriodMs, TimeUnit.MILLISECONDS))
                .flatMap {
                    currencyRepository.getExchangeReference(base).toFlowable()
                }.onBackpressureLatest()
    }
}