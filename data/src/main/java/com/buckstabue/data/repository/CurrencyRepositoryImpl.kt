package com.buckstabue.data.repository

import com.buckstabue.data.api.CurrencyApiInterface
import com.buckstabue.data.entity.dto.ExchangeReferenceDTO
import io.reactivex.Single
import javax.inject.Inject

class CurrencyRepositoryImpl @Inject constructor(
        private val currencyApiInterface: CurrencyApiInterface) : CurrencyRepository {
    override fun getExchangeReference(base: String): Single<ExchangeReferenceDTO> {
        if (base.isEmpty()) {
            return currencyApiInterface.getDefaultExchangeReference()
        } else {
            return currencyApiInterface.getExchangeReference(base)
        }
    }
}