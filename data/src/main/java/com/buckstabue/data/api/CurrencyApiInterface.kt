package com.buckstabue.data.api

import com.buckstabue.data.entity.dto.ExchangeReferenceDTO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApiInterface {

    @GET("latest")
    fun getExchangeReference(@Query("base") base: String): Single<ExchangeReferenceDTO>

    @GET("latest")
    fun getDefaultExchangeReference(): Single<ExchangeReferenceDTO>
}